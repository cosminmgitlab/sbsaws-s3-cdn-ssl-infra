Clone/Fork repo deploy it via gitlabCI/manually with terraform terraform	>= 0.12.0, < 0.14.0

Involved:
route53 - dns mapping of website 
cloudfront - distribution over edge locations
certificate manager - ssl 
s3 - static content of website



AWS credentials setup
You need administrator access on the AWS account to run the Terraform scripts.
Get the AWS Access Key ID and Secret Access Key from your administrator and setup your AWS CLI credentials with a new profile.
This will allow you to run Terraform scripts targeting different AWS accounts based on the profile credentials.
Set the AWS Access Key ID, Secret Access Key, and the default region name.
$ aws configure --profile my-project
AWS Access Key ID [None]: XXX
AWS Secret Access Key [None]: XXX
Default region name [None]: us-east-1
Default output format [None]:




Network:
S3 bucket for each environment or Terraform script.


Create the bucket.

 $ aws s3api create-bucket \
--bucket my-project-my-environment-tf-state \
--acl private \
--region bucket-region \
--create-bucket-configuration LocationConstraint=bucket-region \
--profile my-project

Block public access.
$ aws s3api put-public-access-block \
--bucket my-project-my-environment-tf-state \
--public-access-block-configuration '{"BlockPublicAcls": true, "IgnorePublicAcls": true, "BlockPublicPolicy": true, "RestrictPublicBuckets": true}' \
--region bucket-region \
--profile my-project

Enable versioning.
$ aws s3api put-bucket-versioning \
--bucket my-project-my-environment-tf-state \
--versioning-configuration Status=Enabled \
--region bucket-region \
--profile my-project

Enable bucket encryption using default AWS KMS key.
 $ aws s3api put-bucket-encryption \
--bucket my-project-my-environment-tf-state \
--server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "aws:kms"}}]}' \
--region bucket-region \
--profile my-project


SBS