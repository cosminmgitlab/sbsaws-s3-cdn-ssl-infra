provider "aws" {
  version = "2.49.0"
  region  = var.aws_region
  profile = "sbspr3"
}

provider "aws" {
  version = "2.49.0"
  region  = "us-east-1"
  profile = "sbspr3"
  alias   = "cert_manager"
}

terraform {
  backend "s3" {
    bucket  = "example-prod-tf-state"
    key     = "website_state"
    region  = "us-east-1"
    profile = "sbspr3"
  }
}
