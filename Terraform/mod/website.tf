data "aws_route53_zone" "example_com" {
  name = "example.com"
}

resource "aws_acm_certificate" "example_com" {
  provider = aws.cert_manager
  domain_name       = "example.com"
  validation_method = "DNS"
  subject_alternative_names = ["*.example.com"]
  lifecycle {
    create_before_destroy = true
  }
}

// the aws_acm_certificate_validation resource does not represent a real-world entity in AWS
// but it simply implements a part of the validation workflow
resource "aws_acm_certificate_validation" "example_com" {
  provider = aws.cert_manager
  certificate_arn = aws_acm_certificate.example_com.arn
  validation_record_fqdns = [aws_route53_record.cert_validation_example_com.fqdn]
}

resource "aws_route53_record" "cert_validation_example_com" {
  provider = aws.cert_manager

  name    = aws_acm_certificate.example_com.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.example_com.domain_validation_options[0].resource_record_type
  records = [aws_acm_certificate.example_com.domain_validation_options[0].resource_record_value]

  zone_id = data.aws_route53_zone.example_com.zone_id
  ttl     = 60
}

module "example_website" {
//  source    = "git::https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn.git?ref=0.23.1"
  source    = "git::https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn.git?ref=0.30.0"
  namespace = "example"
  stage     = "prod"
  name      = "website"

  parent_zone_id = data.aws_route53_zone.example_com.zone_id
  aliases = [
    "example.com", "www.example.com"
  ]
  acm_certificate_arn = aws_acm_certificate_validation.example_com.certificate_arn

  website_enabled          = true
  use_regional_s3_endpoint = true
  origin_force_destroy     = false
  cors_allowed_headers     = ["*"]
  cors_allowed_methods     = ["GET", "HEAD", "PUT"]
  cors_expose_headers      = ["ETag"]
  cors_max_age_seconds     = 3600

  // important: these settings control for how long the files will be cached at the edge location
  // if you want your **browsers** to do local cache, you need to set the cache-control header for your files
  // when using s3 as the origin, you can do this by using the `--cache-control` flag when synching files to s3
  // `aws s3 sync . s3://clarrow-prod-website-origin/ --delete --cache-control max-age=31536000`
  default_ttl = 0
  min_ttl     = 0
  compress    = true

  custom_error_response = [
    {
      error_code = 404
      response_code = 200
      response_page_path = "/index.html"
      error_caching_min_ttl = 300
    },
  ]

  lambda_function_association =  [
    {
      event_type   = "viewer-request"
      lambda_arn   = "arn:aws:lambda:us-east-1:114205654795:function:redirect-url-to-www:5"
      include_body = false
    }
  ]

  logging_enabled = false
}



