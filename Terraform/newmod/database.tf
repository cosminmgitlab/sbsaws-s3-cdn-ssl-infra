aws_account_id = "" # AWS Account ID
aws_profile    = "" # AWS credentials profile
aws_region     = "" # AWS EKS cluster region

tag_environment = "" # Value for the Env tag, e.g. MyProductMyEnvironment

rds_instance_name = "myproduct-test-db" # Name of the RDS instance
instance_class    = "db.t3.small" # RDS instance class, see https://aws.amazon.com/rds/instance-types/
allocated_storage = 10 # how much disk space to allocate for the instance

# you can get these values from the output of the network script
# cd terraform/env/<environment>/network && terraform output 
db_subnet_group_name = "" # value of database_subnet_group
security_group_ids = [
  "sg-XXX1", # value of eks_mysql_security_group_id
  "sg-XXX2", # value of devops_mysql_security_group_id
  "sg-XXXN", # any other security group id that should have access to the RDS instance
]

publicly_accessible           = true # the RDS instance is accessible from the Internet but only from the IP addresses listed in the security groups listed in security_group_ids
deletion_protection     = false # if true, databases cannot be deleted - we recommend to enable it for production databases
backup_retention_period = 7 # backup retation period - we recommend setting the max value 30 for production databases

